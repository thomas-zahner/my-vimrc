" Highlighting
syntax on

" Avoid the escape key
imap fd <ESC>

" Enable easy navigation through wrapped lines
noremap j gj
noremap k gk

set relativenumber

" Searching
set ignorecase
set smartcase
set incsearch
set hlsearch

nnoremap <CR> :nohlsearch<cr>

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
map <c-space> ?

" Enable :W and :Q
command! W :w
command! Q :q

" Shows available options
set wildmenu

" Show position
set ruler

" Height of the command bar
set cmdheight=2

" Add a bit extra margin to the left
set foldcolumn=1

" Use spaces for indent
set expandtab
set smarttab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2

set autoindent
set smartindent
set wrap

" Remap VIM 0 to first non-blank character
map 0 ^

" Enable mouse support
set mouse=a
